﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace ChangeRegistryWin32PrioritySeparation_FPSGame.utils
{
    public class RegistryAction
    {
        /// <summary>
        /// 注册表分支
        /// </summary>
        public enum RegistryMode
        {
            HKEY_CLASSES_ROOT,
            HKEY_CURRENT_USER,
            HKEY_LOCAL_MACHINE,
            HKEY_USER,
            HKEY_CURRENT_CONFIG,
        }
        private readonly RegistryKey RegMode = null;
        public RegistryAction(RegistryMode _registryMode)
        {
            //打开指定的注册表分支，选择主键
            RegMode = _registryMode switch
            {
                RegistryMode.HKEY_CLASSES_ROOT => Registry.ClassesRoot,
                RegistryMode.HKEY_CURRENT_USER => Registry.CurrentUser,
                RegistryMode.HKEY_LOCAL_MACHINE => Registry.LocalMachine,
                RegistryMode.HKEY_USER => Registry.Users,
                RegistryMode.HKEY_CURRENT_CONFIG => Registry.CurrentConfig,
                _ => null
            };
        }
        /// <summary>
        /// 获取注册表键值
        /// </summary>
        /// <param name="keyPath">注册表路径</param>
        /// <param name="valueName">键名</param>
        /// <returns></returns>
        public string GetRegistryKeyValue(string keyPath, string valueName)
        {
            // 打开指定的注册表分支
            RegistryKey key = null;
            // 这里以HKEY_LOCAL_MACHINE为例，根据实际情况选择适当的主键
            key = RegMode.OpenSubKey(keyPath,false);

            // 检查键是否存在
            if (key != null)
            {
                // 尝试获取指定的键值
                object keyValue = key.GetValue(valueName);

                // 检查键值是否存在
                if (keyValue != null)
                {
                    // 返回键值的字符串表示形式
                    return keyValue.ToString();
                }
                else
                {
                    return ($"注册表键 '{valueName}' 在路径 {keyPath} 下不存在或其值为空");
                }
            }
            else
            {
                return ($"注册表路径 '{keyPath}' 不存在");
            }
            
        }
        /// <summary>
        /// 更改注册表键值
        /// </summary>
        /// <param name="keyPath">注册表路径</param>
        /// <param name="valueName">键名</param>
        /// <param name="newValue">更改的键值</param>
        public void ChangeRegistryValue(string keyPath, string valueName, int newValue)
        {
            try
            {
                // 打开或创建注册表键
                using (RegistryKey key = RegMode.OpenSubKey(keyPath, true))
                {
                    // 检查键是否存在，如果不存在则抛出异常或创建键（这里假设键已经存在）
                    if (key == null)
                    {
                        throw new Exception($"注册表键路径'{keyPath}'不存在");
                    }

                    // 修改或设置DWORD类型的键值
                    key.SetValue(valueName, newValue, RegistryValueKind.DWord);

                    // 关闭键
                    // 注：此处由于使用了using语句，所以在结束块时会自动关闭
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show($"更改注册表键值时出错：{ex.Message}");
                throw new ArgumentException($"更改注册表键值时出错：{ex.Message}");
            }
        }

    }
}

