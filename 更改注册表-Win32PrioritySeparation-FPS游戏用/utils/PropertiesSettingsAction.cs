﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Text.Json;
using System.Windows.Forms;
using ChangeRegistryWin32PrioritySeparation_FPSGame.model;

namespace ChangeRegistryWin32PrioritySeparation_FPSGame.utils
{
    class PropertiesSettingsAction
    {
        public static BindingList<win32PrioritySeparationValue> GetWin32PSV()
        {
            BindingList<win32PrioritySeparationValue> win32PSVS = new BindingList<win32PrioritySeparationValue>();
            try
            {
               win32PSVS = JsonSerializer.Deserialize<BindingList<win32PrioritySeparationValue>>(Properties.Settings.Default.win32PSVs);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
            return win32PSVS;
        }

        public static void ChangeWin32PSV(win32PrioritySeparationValue item)
        {
            try
            {
                var win32PSVS = GetWin32PSV();
                win32PSVS.Add(item);
                var tempStr = JsonSerializer.Serialize(win32PSVS);
                Properties.Settings.Default.win32PSVs = tempStr;
                Properties.Settings.Default.Save();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
        }
    }
}
