﻿using System;
using System.Collections.Generic;
using System.Security.Principal;
using System.Text;

namespace ChangeRegistryWin32PrioritySeparation_FPSGame.utils
{
    class util
    {
        /// <summary>
        /// 判断程序是否是以管理员身份运行。
        /// </summary>
        public static bool IsRunAsAdmin()
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(id);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }
    }
}
