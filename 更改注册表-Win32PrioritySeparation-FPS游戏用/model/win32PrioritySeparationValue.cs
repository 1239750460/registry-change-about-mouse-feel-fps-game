﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChangeRegistryWin32PrioritySeparation_FPSGame.model
{
    class win32PrioritySeparationValue
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title{ get; set; }
        /// <summary>
        /// 十进制值
        /// </summary>
        public int Val { get; set; }
        /// <summary>
        /// 十六进制值
        /// </summary>
        public string Hex { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Desc { get; set; }

    }
}
