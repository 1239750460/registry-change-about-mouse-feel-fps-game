﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChangeRegistryWin32PrioritySeparation_FPSGame.model;
using ChangeRegistryWin32PrioritySeparation_FPSGame.utils;
using static ChangeRegistryWin32PrioritySeparation_FPSGame.utils.RegistryAction;

namespace ChangeRegistryWin32PrioritySeparation_FPSGame
{
    public partial class windform : Form
    {
        private Properties.Settings AppSettings = Properties.Settings.Default;
        BindingList<win32PrioritySeparationValue> win32PSVS = new BindingList<win32PrioritySeparationValue>();
        private readonly ToolTip tip = new ToolTip();
        private bool comboboxDropIsDown = false;
        // 获取注册表键值
        private readonly RegistryAction registry = new RegistryAction(RegistryMode.HKEY_LOCAL_MACHINE);
        public windform()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if(!util.IsRunAsAdmin())
            {
                MessageBox.Show("由于涉及到注册表操作，请使用管理员权限运行本程序！");
                Process.GetCurrentProcess().Kill();
            }
            this.Icon = Properties.Resources.logo;
            win32PSVS = PropertiesSettingsAction.GetWin32PSV();
            var a = Properties.Settings.Default;
            //string systemPath = @"SYSTEM\CurrentControlSet\Control\PriorityControl";
            //string valueToRead = "Win32PrioritySeparation";
            int valueFromRegistry = Convert.ToInt32(registry.GetRegistryKeyValue(AppSettings.RegistryPath,AppSettings.RegistryValueToRead));
            comboBox1.Items.Clear();
            comboBox1.DataSource = win32PSVS;
            int cbIndex = win32PSVS.IndexOf(win32PSVS.FirstOrDefault(d => d.Val == valueFromRegistry));
            if (cbIndex == -1)
            {
                PropertiesSettingsAction.ChangeWin32PSV(new win32PrioritySeparationValue
                {
                    Title = valueFromRegistry.ToString(),
                    Val = valueFromRegistry,
                    Hex = valueFromRegistry.ToString("X"),
                    Desc = "用户注册表初始值"
                });
                //更新设置后重新获取
                win32PSVS = PropertiesSettingsAction.GetWin32PSV();
                cbIndex = 0;
            }
            comboBox1.SelectedIndex =cbIndex;
            /*comboBox1.DisplayMember = "Title";
            comboBox1.ValueMember = "Val";*/
            pictureBox1.Image = Properties.Resources.win32PSVS;
           
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //Process.Start("https://www.bilibili.com/video/BV1uz421Q74A");
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "https://www.bilibili.com/video/BV1uz421Q74A"; // 要启动的文件或程序名
            startInfo.UseShellExecute = true; // 设置为true
            // 创建并启动进程
            Process process = new Process();
            process.StartInfo = startInfo;
            process.Start();
        }

        private void comboBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            // 绘制背景
            e.DrawBackground();
            //绘制列表项目
            e.Graphics.DrawString((comboBox1.Items[e.Index] as win32PrioritySeparationValue).Val.ToString(), e.Font, Brushes.Black, e.Bounds);
            //将高亮的列表项目的文字传递到tip(之前建立roolTip的一个实例)
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected && comboboxDropIsDown)    
            {
                tip.Show((comboBox1.Items[e.Index] as win32PrioritySeparationValue).Desc, comboBox1,
                    e.Bounds.X + e.Bounds.Width + 5, e.Bounds.Y + e.Bounds.Height + 5);
            }
            e.DrawFocusRectangle();
        }
        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            comboboxDropIsDown = true;
        }

        private void comboBox1_DropDownClosed(object sender, EventArgs e)
        {
            comboboxDropIsDown = false;
            tip.Hide(comboBox1);
            
        }
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                var valueFromRegistry = win32PSVS[comboBox1.SelectedIndex].Val;
                registry.ChangeRegistryValue(AppSettings.RegistryPath, AppSettings.RegistryValueToRead, valueFromRegistry);
                MessageBox.Show("更改成功");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);                
            }
        }

        

        
    }
}
